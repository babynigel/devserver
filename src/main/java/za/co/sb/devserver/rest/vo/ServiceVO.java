package za.co.sb.devserver.rest.vo;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 *
 * @author steven
 */
public class ServiceVO {
    
    @NotNull
    @Schema(description = "Unique name of your service.")
    private String serviceName;
       
    @Email(message = "Email address is invalid")
    @Schema(description = "Email address used to notify of any issues with the server")
    private String email;
    
    @NotNull    
    @Schema(description = "The url to invoke in order to register the list of servers with your server + /register", example = "http://myserverip:8080/mycontext")
    private String registrationUrl;
    
    @NotNull    
    @Schema(description = "The url to invoke in order to inform your server that the state of the andon cord has changed + /andoncord", example = "http://myserverip:8080/mycontext")
    private String andonCordStateChangeUrl;
    
    @NotNull   
    @Schema(description = "The Location of your server on the floor. (Closest pillar).", example = "C")
    private String location;
        
    @Schema(description = "The url optiontional URL to invoke in order to retreive metric information about your server + /metric", example = "http://myserverip:8080/mycontext")
    private String metricsUrl;
    
    @NotNull
    @Schema(description = "The url to invoke in order to determine if your server is healthy or not", example = "http://myserverip:8080/mycontext")
    private String healthCheckUrl;
    
    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }    
    
    public String getRegistrationUrl() {
        return registrationUrl;
    }

    public void setRegistrationUrl(String registrationUrl) {
        this.registrationUrl = registrationUrl;
    }

    public String getAndonCordStateChangeUrl() {
        return andonCordStateChangeUrl;
    }

    public void setAndonCordStateChangeUrl(String andonCordStateChangeUrl) {
        this.andonCordStateChangeUrl = andonCordStateChangeUrl;
    }

    public String getMetricsUrl() {
        return metricsUrl;
    }

    public void setMetricsUrl(String metricsUrl) {
        this.metricsUrl = metricsUrl;
    }

    public String getHealthCheckUrl() {
        return healthCheckUrl;
    }

    public void setHealthCheckUrl(String healthCheckUrl) {
        this.healthCheckUrl = healthCheckUrl;
    }

    
}
