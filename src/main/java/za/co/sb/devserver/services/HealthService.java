package za.co.sb.devserver.services;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import org.eclipse.microprofile.rest.client.RestClientBuilder;
import za.co.sb.devserver.clients.feature.Health;
import za.co.sb.devserver.clients.feature.HealthClient;
import za.co.sb.devserver.entity.Register;

/**
 * This service is responsible for all the health checks against the registered
 * servers.
 *
 * @author steven
 */
@Stateless
public class HealthService {

    Logger log = Logger.getLogger("HealthService");

    @Inject
    RegisterService registerService;

    @Inject
    MailService mailService;

    /**
     * Performs a health check on all the registered servers.
     */
    public void doHealthChecks() {
        log.info("Performing health checks");
        List<Register> regList = registerService.getAllRegisteredServers();
        for (Register reg : regList) {
            log.log(Level.INFO, "Health check for service {0}", reg.getServiceName());
            checkHealth(reg);
        }
    }

    /**
     * Calls the health check method on the supplied Service to determine if the
     * service is healthy and responding. Sends emails of any problems encountered.
     * @param reg The details of the server to perform the health check on.
     */
    private void checkHealth(Register reg) {
        HealthClient client
                = RestClientBuilder.newBuilder()
                        .baseUrl(reg.getHealthCheckUrl())
                        .build(HealthClient.class);
        try {
            Response resp = client.health();
            if (resp.getStatus() != 200) {
                log.log(Level.WARNING, "Health Check for server {0} returned a HTTP status of {1}", new Object[]{reg.getServiceName(), resp.getStatus()});
                mailService.notifyOfHealthIssues(reg, resp.getStatus());
            } else {
                Health health = resp.readEntity(Health.class);
                if (health != null) {
                    if (health.getOutcome() != null) {
                        if (!health.getOutcome().equals("UP")) {
                            log.log(Level.WARNING, "Health Check for server {0} returned an outcome of {1}", new Object[]{reg.getServiceName(), health.getOutcome()});
                            mailService.notifyOfHealthIssues(reg, health);
                        }
                    } else {
                        log.log(Level.WARNING, "Health Check for server {0} returned a null outcome value}", new Object[]{reg.getServiceName()});
                        mailService.notifyOfHealthIssues(reg, "Health Check returned a null outcome value");
                    }
                } else {
                    log.log(Level.WARNING, "Health Check for server {0} returned an empty health object}", new Object[]{reg.getServiceName()});
                    mailService.notifyOfHealthIssues(reg, "Health Check returned an empty health object");
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Unable to perform a healthcheck on " + reg.getServiceName(), e);            
            mailService.notifyOfHealthIssues(reg, e);
        }
    }

}
